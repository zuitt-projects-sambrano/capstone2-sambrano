const express = require('express');

const router = express.Router();

const productControllers = require('../controllers/productControllers');

const auth = require("../auth");

const {verify, verifyAdmin} = auth;

router.post('/', verify, verifyAdmin, productControllers.addProduct);

router.get('/', productControllers.getAllProducts);

router.get('/getSingleProduct/:id', productControllers.getSingleProductControllers);

router.put('/:id', verify, verifyAdmin, productControllers.updateProduct);

router.put('/archieve/:id', verify, verifyAdmin, productControllers.archieve);

module.exports = router;
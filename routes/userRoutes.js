const express = require('express');

const router = express.Router();

const userControllers = require('../controllers/userControllers');

const auth = require("../auth");

const {verify, verifyAdmin} = auth;

router.post("/", userControllers.registerUser);

router.get("/", userControllers.getAllUsers);

router.post("/login", userControllers.loginUser);

router.put("/updateAdmin/:id", verify, verifyAdmin, userControllers.updateAdmin);

router.post("/orders", verify, userControllers.orders);

router.get("/getUserOrders", verify, userControllers.getUserOrders);

router.get("/getAllOrders/:id", verify, verifyAdmin, userControllers.getAllOrders);

module.exports = router;
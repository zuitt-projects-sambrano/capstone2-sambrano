const express = require('express');

const mongoose = require('mongoose');

const cors = require('cors');

const userRoutes = require('./routes/userRoutes')

const productRoutes = require('./routes/productRoutes')

const port = process.env.PORT || 4000;

const app = express();

mongoose.connect("mongodb+srv://admin_sambrano:admin169@batch-169.gdgwg.mongodb.net/capstone2-sambrano?retryWrites=true&w=majority", {

    useNewUrlParser: true,
    useUnifiedTopology: true
});

let db = mongoose.connection;

db.on('error', console.error.bind(console, 'Connection Error'));

db.once('open', () => console.log('Connected to MongoDB'));

app.use(express.json());

app.use(cors());

app.use('/users', userRoutes);

app.use('/products', productRoutes);

app.listen(port, () => console.log(`Server is running at port ${port}`))

//https://gentle-badlands-46562.herokuapp.com/
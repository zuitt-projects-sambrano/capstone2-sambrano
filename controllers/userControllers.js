const bcrypt = require("bcrypt");

const User = require("../models/User");

const Product = require("../models/Product");

const auth = require("../auth");

module.exports.registerUser = (req, res) => {

	console.log(req.body);

	User.findOne({email: req.body.email})
	.then(result => {
		if(result !== null && result.email === req.body.email){
			return res.send("Email Address is already been taken.")
		} else {
			const hashedPW = bcrypt.hashSync(req.body.password, 10);

			let newUser = new User({

				firstName: req.body.firstName,
				lastName: req.body.lastName,
				email: req.body.email,
				password: hashedPW
			})

			newUser.save()
			.then(user => res.send(user))
			.catch(err => res.send(err));
		}
	});

};

module.exports.getAllUsers = (req, res) => {

	User.find({})
		.then(result => res.send(result))
		.catch(err => res.send(err));
};

module.exports.loginUser = (req, res) => {

	console.log(req.body);

	User.findOne({
			email: req.body.email
		})
		.then(foundUser => {

			if (foundUser === null) {

				return res.send("User does not exist");

			} else {

				const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password);

				if (isPasswordCorrect) {

					return res.send({
						accessToken: auth.createAccessToken(foundUser)
					});

				} else {

					return res.send("Password is incorrect");
				}
			}
		})
		.catch(err => res.send(err));
};

module.exports.updateAdmin = (req, res) => {

	console.log(req.user.id);
	console.log(req.params.id);

	let updates = {
		isAdmin: true
	};

	User.findByIdAndUpdate(req.params.id, updates, {
			new: true
		})
		.then(updateUser => res.send(updateUser))
		.catch(err => res.send(err));
};

module.exports.orders = async (req, res) => {

    console.log(req.user.id);
    console.log(req.body.productId);

    if(req.user.isAdmin){
        return res.send("Action Forbidden");
    }

        let isUserUpdated = await User.findById(req.user.id).then(user => {

            console.log(user);

            let newOrder = {
                productId: req.body.productId,
				totalAmount: req.body.totalAmount
            }

            user.orders.push(newOrder);

            return user.save().then(user => true).catch(err => err.message);
        });

        if(isUserUpdated !== false){
            return res.send({message: 'Ordered Successfully!'})
        };

        let isProductUpdated = await Product.findById(req.body.productId).then(product => {

            console.log(product);

			let purchaser = {
                userId: req.user.id
            }

            product.purchaser.push(purchaser);

            return product.save().then(product => true).catch(err => err.message);
        });

        if(isProductUpdated !== true){
            return res.send({message: isProductUpdated})
        };

        if(isUserUpdated && isProductUpdated){
            return res.send({message: 'Ordered Successfully!'})
        };
};

module.exports.getUserOrders = (req, res) => {

    console.log(req.user)

    User.findById(req.user.id)
    .then(result => res.send(result.orders))
    .catch(err => res.send(err))
};

module.exports.getAllOrders = (req, res) => {

    User.findById(req.params.id)
    .then(result => res.send(result.orders))
    .catch(err => res.send(err))
};